import hashlib
from pathlib import Path
from typing import List, Union

import pandas as pd


def write_sample_annotation_output_file(sample_annotation_file: Union[str, Path],
                                        sample_files: List[Union[str, Path]]) -> None:
    with open(sample_annotation_file, "w") as sample_annotation:
        for i, sample_file in enumerate(sample_files):
            with open(sample_file, "r") as sample:
                line = next(sample)
                if i == 0:
                    sample_annotation.write(line)
                line = next(sample)
                sample_annotation.write(line)


def concat_columns(files, start_column: int = 1):
    accumulated_df = None
    for i, file in enumerate(files):
        current_df = pd.read_csv(file, sep='\t')
        if i == 0:
            accumulated_df = current_df
            continue
        accumulated_df[current_df.columns[start_column:]] = current_df.iloc[:, start_column:]
    return accumulated_df


def check_if_files_identical(files: List[Union[str, Path]]):
    first_md5 = None
    for i, file in enumerate(files):
        with open(file, 'r') as f:
            content = ''.join(f.readlines())
        if i == 0:
            first_md5 = hashlib.md5(content.encode('utf-8')).hexdigest()
            continue
        current_md5 = hashlib.md5(content.encode('utf-8')).hexdigest()

        if first_md5 != current_md5:
            return False
    return True
