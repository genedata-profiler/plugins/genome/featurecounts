#!/bin/bash

export env_dir="$plugin_folder/conda_env"

# Unpack the plugin's conda environment if needed
if [[ ! -d "$env_dir" ]]
then
    mkdir "$env_dir"
    {
      tar -xzf "$plugin_folder"/conda_env.tar.gz -C "$env_dir" --no-same-owner
      "$env_dir"/bin/conda-unpack
    } || {
      # delete the env dir in case there was a problem
      rm -r "$env_dir"
      echo "There was an issue with unpacking the Conda environment!"
      exit 1
    }
fi

# Activate the custom environment
source "$env_dir"/bin/activate
