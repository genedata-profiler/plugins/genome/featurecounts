#!/bin/bash

source "${plugin_folder}/utils/setup_conda_env.sh"

# Extend default search path with plugin folder and utils
export PYTHONPATH="$plugin_folder"

python "$@"
