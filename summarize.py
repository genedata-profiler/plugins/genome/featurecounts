import os

import pandas as pd

import shared

##############################################################################
# Read Environment Variables
##############################################################################

sharedFolder = os.getenv("shared_folder")
will_export = os.getenv("EXPORT_RESULTS") == 'true'
export_folder = os.getenv('EXPORT_FOLDER')
generic_output_layer = os.getenv('output_generic_data_0')
num_samples = int(os.getenv('total_samples_0'))
track_names = [os.getenv(f"track_name_0_{i}") for i in range(num_samples)]

##############################################################################
# Setup Paths
##############################################################################

annotation_output_file = os.path.join(sharedFolder, "0_sample_annotation_output.txt")
sample_files = [os.path.join(sharedFolder, track_name, "sample_annotation_output.txt") for track_name in track_names]
summary_files = [os.path.join(sharedFolder, track_name, "counts.summary") for track_name in track_names]
annotation_files = [os.path.join(sharedFolder, track_name, "annotation.tsv") for track_name in track_names]
counts_files = [os.path.join(sharedFolder, track_name, "counts.tsv") for track_name in track_names]

##############################################################################
# Create tables
##############################################################################

counts_table: pd.DataFrame = shared.concat_columns(annotation_files[0:1] + counts_files)
summary_table: pd.DataFrame = shared.concat_columns(summary_files)

##############################################################################
# Write Files
##############################################################################

shared.write_sample_annotation_output_file(annotation_output_file, sample_files)

if will_export:
    counts_table.to_csv(os.path.join(export_folder, 'counts.tsv.gz'), compression='gzip', sep='\t', index=False)
    summary_table.to_csv(os.path.join(export_folder, 'counts.summary'), sep='\t', index=False)

if generic_output_layer:
    counts_table.to_csv(os.path.join(generic_output_layer, "feature_counts", 'counts.tsv.gz'), compression='gzip', sep='\t', index=False)
    summary_table.to_csv(os.path.join(generic_output_layer, "feature_counts", 'counts.summary'), sep='\t', index=False)
